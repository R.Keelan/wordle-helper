﻿open System
open System.IO

// Word Likelihood --------------------------------------------------------------------------------

let letterFrequencies (words:List<string>) : Map<char,int> =
    let addOrInc (map:Map<char,int>) c =
        map.Change(c, (fun value -> if value.IsNone then Some(1) else Some(value.Value+1)))

    let mapWordToChars (map:Map<char,int>) (word:string) : Map<char,int> = 
        word.ToCharArray() |> Array.fold (fun map c -> addOrInc map c) map

    words |> List.fold (fun map word -> mapWordToChars map word) Map.empty

let wordScore (word:string) (frequencies:Map<char,int>) : int =
    word.ToCharArray() |> Array.distinct |> Array.sumBy (fun c -> frequencies[c])

let wordScores (words:List<string>) : Map<string,int> =
    let frequencies = letterFrequencies words
    words |> List.fold (fun map word -> map.Add(word, (wordScore word frequencies))) Map.empty

// Filters ----------------------------------------------------------------------------------------

let greenFilter (word:string) (guess:string) (greenLetters:string) : bool =
    let word = word.ToCharArray()
    let greenLetters = greenLetters.ToCharArray()
    let indices = greenLetters |> Array.map (fun c -> guess.IndexOf(c))
    (greenLetters, indices) ||> Seq.forall2 (fun c i -> word[i] = c)

let yellowFilter (word:string) (guess:string) (yellowLetters:string) : bool =
   let word = word.ToCharArray()
   let yellowLetters = yellowLetters.ToCharArray()
   let indices = yellowLetters |> Array.map (fun c -> guess.IndexOf(c))
   (yellowLetters, indices) ||> Seq.forall2 (fun c i -> (word[i] <> c) && (word |> Array.contains c))

let greyFilter (word:string) (greyLetters:string) : bool =
    greyLetters |> Seq.forall (fun c -> not (word.Contains(c)))

let guess (words:List<string>) : List<string> =
    let mutable candidates = words
    printfn "Enter your guess:"
    let guess = Console.ReadLine()

    printfn "Green letters?"
    let greenLetters = Console.ReadLine()
    
    printfn "Yellow letters?"
    let yellowLetters = Console.ReadLine()

    let greyLetters = guess.ToCharArray()
                    |> Array.except (greenLetters.ToCharArray())
                    |> Array.except (yellowLetters.ToCharArray())

    if greenLetters.Length > 0 then do
        candidates <- candidates |> List.filter (fun w -> greenFilter w guess greenLetters)

    if yellowLetters.Length > 0 then do
        candidates <- candidates |> List.filter (fun w -> yellowFilter w guess yellowLetters)
        
    if greyLetters.Length > 0 then do
        candidates <- candidates |> List.filter (fun w -> greyFilter w (new string(greyLetters)))

    candidates |> List.except (seq {guess})


[<EntryPoint>]
let main argv =
    let mutable words = File.ReadLines @"Words.txt" |> Seq.toList
    while List.length words > 0 do
        words <- guess words
        let wordScores = wordScores words
        words <- words |> List.sortByDescending (fun w -> wordScores[w])
        printfn "%s" (String.Join("\n", words |> List.truncate 10))
    exit 0